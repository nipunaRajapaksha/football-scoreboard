import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for Match. It tests the creation, score updating, and other functionalities of the Match class.
 */
public class MatchTest {

    /**
     * Test to ensure that a newly created match starts with scores of zero.
     */
    @Test
    public void shouldCreateMatchWithInitialScoresZero() {
        Match match = new Match("HomeTeam", "AwayTeam");
        assertEquals(0, match.getHomeScore());
        assertEquals(0, match.getAwayScore());
    }

    /**
     * Test to verify that the match scores are correctly updated.
     */
    @Test
    public void shouldUpdateMatchScores() {
        Match match = new Match("HomeTeam", "AwayTeam");
        match.updateScore(2, 3);
        assertEquals(2, match.getHomeScore());
        assertEquals(3, match.getAwayScore());
    }

    /**
     * Test to confirm that the total score is calculated correctly.
     */
    @Test
    public void shouldReturnTotalScore() {
        Match match = new Match("HomeTeam", "AwayTeam");
        match.updateScore(2, 3);
        assertEquals(5, match.getTotalScore());
    }

    /**
     * Test to verify that the start time of the match is set correctly at the time of match creation.
     */
    @Test
    public void shouldReturnCorrectStartTime() {
        LocalDateTime beforeStart = LocalDateTime.now();
        Match match = new Match("HomeTeam", "AwayTeam");
        LocalDateTime afterStart = LocalDateTime.now();

        LocalDateTime startTime = match.getStartTime();
        assertTrue(startTime.isAfter(beforeStart) || startTime.isEqual(beforeStart));
        assertTrue(startTime.isBefore(afterStart) || startTime.isEqual(afterStart));
    }

    /**
     * Test to ensure that creating a match with invalid team names throws an exception.
     */
    @Test
    public void shouldThrowExceptionForInvalidTeamNames() {
        assertThrows(IllegalArgumentException.class, () -> new Match(null, "AwayTeam"));
        assertThrows(IllegalArgumentException.class, () -> new Match("HomeTeam", ""));
    }

    /**
     * Test to verify that updating the match score to negative values throws an exception.
     */
    @Test
    public void shouldThrowExceptionForNegativeScores() {
        Match match = new Match("HomeTeam", "AwayTeam");
        assertThrows(IllegalArgumentException.class, () -> match.updateScore(-1, 1));
    }

}
