import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class for Scoreboard. It tests functionalities like adding, updating, and finishing matches, as well as generating summaries.
 */
public class ScoreboardTest {

    /**
     * Test to ensure that a match can be successfully added to the scoreboard.
     */
    @Test
    public void shouldAddMatchToScoreboard() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("HomeTeam", "AwayTeam");
        assertEquals(1, scoreboard.getNumberOfMatches());
    }

    /**
     * Test to confirm that the scoreboard correctly updates the score of a match.
     */
    @Test
    public void shouldUpdateScoreOfMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("HomeTeam", "AwayTeam");
        scoreboard.updateScore("HomeTeam", "AwayTeam", 2, 1);
        Match updatedMatch = scoreboard.findMatch("HomeTeam", "AwayTeam");
        assertEquals(2, updatedMatch.getHomeScore());
        assertEquals(1, updatedMatch.getAwayScore());
    }

    /**
     * Test to ensure that a match can be correctly finished and removed from the scoreboard.
     */
    @Test
    public void shouldRemoveMatchOnFinish() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("HomeTeam", "AwayTeam");
        scoreboard.finishMatch("HomeTeam", "AwayTeam");
        assertEquals(0, scoreboard.getNumberOfMatches());
    }

    /**
     * Test to verify that the getSummary method returns matches ordered by total score and most recent start time.
     */
    @Test
    public void shouldReturnMatchesOrderedByTotalScoreAndStartTime() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("Mexico", "Canada");  // Assume each startMatch records start time
        scoreboard.updateScore("Mexico", "Canada", 0, 5);
        scoreboard.startMatch("Spain", "Brazil");
        scoreboard.updateScore("Spain", "Brazil", 10, 2);
        scoreboard.startMatch("Germany", "France");
        scoreboard.updateScore("Germany", "France", 2, 2);
        scoreboard.startMatch("Uruguay", "Italy");
        scoreboard.updateScore("Uruguay", "Italy", 6, 6);
        scoreboard.startMatch("Argentina", "Australia");
        scoreboard.updateScore("Argentina", "Australia", 3, 1);

        List<Match> summary = scoreboard.getSummary();
        assertEquals("Uruguay 6 - Italy 6", summary.get(0).toString()); // Highest total score
        assertEquals("Spain 10 - Brazil 2", summary.get(1).toString()); // Next highest total score
        assertEquals("Mexico 0 - Canada 5", summary.get(2).toString());
        assertEquals("Argentina 3 - Australia 1", summary.get(3).toString());
        assertEquals("Germany 2 - France 2", summary.get(4).toString());
    }

    /**
     * Tests if the match summary is correctly ordered by total score and, in case of a tie, by start time.
     */
    @Test
    public void shouldCorrectlyOrderMatchesWithSameScores() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("Spain", "Brazil");
        scoreboard.updateScore("Spain", "Brazil", 10, 2);
        scoreboard.startMatch("Uruguay", "Italy");
        scoreboard.updateScore("Uruguay", "Italy", 6, 6);

        List<Match> summary = scoreboard.getSummary();
        assertEquals("Uruguay 6 - Italy 6", summary.get(0).toString()); // Highest total score
        assertEquals("Spain 10 - Brazil 2", summary.get(1).toString()); // Next highest total score
    }

    /**
     * Ensures that attempting to start a match that already exists throws an exception.
     */
    @Test
    public void shouldNotAllowStartingDuplicateMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("HomeTeam", "AwayTeam");

        assertThrows(IllegalArgumentException.class, () -> {
            scoreboard.startMatch("HomeTeam", "AwayTeam");
        });
    }

    /**
     * Verifies that updating the score for a non-existent match throws an exception.
     */
    @Test
    public void shouldNotAllowScoreUpdateForNonexistentMatch() {
        Scoreboard scoreboard = new Scoreboard();

        assertThrows(IllegalArgumentException.class, () -> {
            scoreboard.updateScore("HomeTeam 1", "AwayTeam 1", 1, 2);
        });
    }

    /**
     * Checks that updating the score for a finished match is not allowed and throws an exception.
     */
    @Test
    public void shouldNotAllowScoreUpdateForFinishedMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("HomeTeam", "AwayTeam");
        scoreboard.updateScore("HomeTeam", "AwayTeam", 1, 2);
        scoreboard.finishMatch("HomeTeam", "AwayTeam");

        assertThrows(IllegalArgumentException.class, () -> {
            scoreboard.updateScore("HomeTeam", "AwayTeam", 2, 2);
        });
    }

    /**
     * Tests the getSummary method to ensure it handles an empty scoreboard correctly.
     */
    @Test
    public void shouldHandleEmptyScoreboardInSummary() {
        Scoreboard scoreboard = new Scoreboard();
        assertTrue(scoreboard.getSummary().isEmpty());
    }

    /**
     * Ensures that attempting to finish a non-existent match throws an exception.
     */
    @Test
    public void shouldNotFinishNonexistentMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("TeamA", "TeamB");
        assertThrows(IllegalArgumentException.class, () -> {
            scoreboard.finishMatch("TeamX", "TeamY");
        });
    }

    /**
     * Verifies that an exception is thrown if there is an attempt to update the score of a finished match.
     */
    @Test
    public void shouldNotUpdateScoreOfFinishedMatch() {
        Scoreboard scoreboard = new Scoreboard();
        scoreboard.startMatch("TeamA", "TeamB");
        scoreboard.finishMatch("TeamA", "TeamB");
        assertThrows(IllegalArgumentException.class, () -> {
            scoreboard.updateScore("TeamA", "TeamB", 1, 2);
        });
    }

}
