import java.time.LocalDateTime;

/**
 * Represents a single football match with teams, scores, and start time.
 */
public class Match {
    private String homeTeam;
    private String awayTeam;
    private int homeScore = 0;
    private int awayScore = 0;

    private LocalDateTime startTime;

    /**
     * Constructs a Match with specified home and away teams.
     * Initializes the match scores to 0-0 and records the start time.
     *
     * @param homeTeam Name of the home team.
     * @param awayTeam Name of the away team.
     * @throws IllegalArgumentException if team names are null or empty.
     */
    public Match(String homeTeam, String awayTeam) {
        if (homeTeam == null || homeTeam.trim().isEmpty() || awayTeam == null || awayTeam.trim().isEmpty()) {
            throw new IllegalArgumentException("Team names cannot be null or empty");
        }
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.startTime = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return homeTeam + " " + homeScore + " - " + awayTeam + " " + awayScore;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    /**
     * Updates the score of the match.
     *
     * @param homeScore New score of the home team.
     * @param awayScore New score of the away team.
     * @throws IllegalArgumentException if scores are negative.
     */
    public void updateScore(int homeScore, int awayScore) {
        if (homeScore < 0 || awayScore < 0) {
            throw new IllegalArgumentException("Scores cannot be negative");
        }
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    public int getTotalScore() {
        return homeScore + awayScore;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

}
