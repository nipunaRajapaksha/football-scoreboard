# Live Football World Cup Scoreboard Library

This Java library provides functionalities to manage live football match scores. It allows users to start new matches, update scores, finish matches, and view a summary of ongoing matches. The library is designed for simplicity and efficiency, following clean code practices and object-oriented principles.

# Features
* Start a Match: Begin tracking a new football match with initial scores set to 0 - 0.
* Update Score: Update the score for any ongoing match.
* Finish Match: Conclude tracking a match and remove it from the scoreboard.
* Get Summary: Retrieve a summary of all ongoing matches, sorted by total score and start time.

# Setup
This library is a standard Java project. It requires Java 17 and can be imported into any Java IDE like IntelliJ IDEA, Eclipse.

1. Clone the repository:
```git clone [repository-url]```
2. Import the project into your IDE:
* Ensure you have Java 17 installed.
* Open the project in your IDE and let it load the Maven dependencies.

# Usage
## Starting a Match
To start a new match:
```
Scoreboard scoreboard = new Scoreboard();
scoreboard.startMatch("HomeTeamName", "AwayTeamName");
```

## Updating a Match Score
To update the score of an ongoing match:
```
scoreboard.updateScore("HomeTeamName", "AwayTeamName", homeScore, awayScore);
```

## Finishing a Match
To finish a match:
```
scoreboard.finishMatch("HomeTeamName", "AwayTeamName");
```

## Getting a Summary of Matches
To get a summary of all ongoing matches:
```
List<Match> ongoingMatches = scoreboard.getSummary();
```

# Testing
The library includes a suite of unit tests written using JUnit 5. These can be run directly within the IDE to validate the functionality.

# Concurrency
Currently, the library does not handle concurrency and is designed for single-threaded applications. If required for multi-threaded environments, additional synchronization mechanisms would need to be implemented.

# Limitations
* The library assumes that each match is uniquely identified by the combination of home and away team names.
* It does not handle persistence; all data is stored in memory and is not saved between application restarts.
* Assumed number of ongoing matches are moderate, not a large number

# Potential Scalability and Performance Concerns
* Large Number of Matches: If the number of matches grows significantly large, linear search operations (finding and checking for duplicates) may become a bottleneck.
* Concurrency and Real-Time Updates: If your system needs to handle frequent real-time updates in a multi-threaded environment, the current data structure might not be the most efficient, especially without concurrency control.

# Future Enhancements
* Implementing thread-safety for use in multi-threaded environments.
* Adding persistence to store and retrieve match data.
* Extending functionalities to include more detailed match statistics.

# Author
Nipuna Rajapaksha
