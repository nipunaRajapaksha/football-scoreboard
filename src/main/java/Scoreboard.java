import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Manages the scoreboard for football matches, allowing to start, update, finish matches and get a summary.
 */
public class Scoreboard {
    private static final Logger logger = Logger.getLogger(Scoreboard.class.getName());
    private List<Match> matches = new ArrayList<>();

    /**
     * Starts a new match and adds it to the scoreboard.
     *
     * @param homeTeam Name of the home team.
     * @param awayTeam Name of the away team.
     * @throws IllegalArgumentException if team names are null, empty, or match already exists.
     */
    public void startMatch(String homeTeam, String awayTeam) {
        if (homeTeam == null || homeTeam.isEmpty() || awayTeam == null || awayTeam.isEmpty()) {
            throw new IllegalArgumentException("Team names cannot be null or empty");
        }
        if (matches.stream().anyMatch(match -> match.getHomeTeam().equals(homeTeam) && match.getAwayTeam().equals(awayTeam))) {
            throw new IllegalArgumentException("Match already exists");
        }
        matches.add(new Match(homeTeam, awayTeam));

        logger.log(Level.INFO, "Started match: {0} vs {1}", new Object[]{homeTeam, awayTeam});
    }

    public void updateScore(String homeTeam, String awayTeam, int homeScore, int awayScore) {
        if (homeScore < 0 || awayScore < 0) {
            throw new IllegalArgumentException("Scores cannot be negative");
        }
        Match match = findMatch(homeTeam, awayTeam);
        if (match == null || !matches.contains(match)) {
            throw new IllegalArgumentException("Match not found or already finished");
        }
        match.updateScore(homeScore, awayScore);

        logger.log(Level.INFO, "Updated score for {0} vs {1} to {2} - {3}", new Object[]{homeTeam, awayTeam, homeScore, awayScore});
    }

    public void finishMatch(String homeTeam, String awayTeam) {
        Match match = findMatch(homeTeam, awayTeam);
        if (match == null) {
            throw new IllegalArgumentException("Cannot finish non-existent match");
        }
        matches.remove(match);

        logger.log(Level.INFO, "Finished match: {0} vs {1}", new Object[]{homeTeam, awayTeam});
    }

    /**
     * Provides a summary of all ongoing matches, sorted first by total score in descending order, then by start time.
     *
     * @return A list of matches sorted as total score, match startTime
     */
    public List<Match> getSummary() {
        return matches.stream()
                .sorted(Comparator.comparingInt(Match::getTotalScore)
                        .reversed()  // First sort by total score in descending order
                        .thenComparing(Match::getStartTime, Comparator.reverseOrder()))  // Then by start time, most recent first
                .toList();
    }

    public Match findMatch(String homeTeam, String awayTeam) {
        return matches.stream()
                .filter(match -> match.getHomeTeam().equals(homeTeam) && match.getAwayTeam().equals(awayTeam))
                .findFirst()
                .orElse(null);
    }

    public int getNumberOfMatches() {
        return matches.size();
    }

    @Override
    public String toString() {
        return "Scoreboard{" +
                "matches=" + matches +
                '}';
    }
}
